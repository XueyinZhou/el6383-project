
Project Review
=====================================================


## Overview

1) Briefly summarize the experiment in this project.

This project compares the performance of Open Shortest Path First (OSPF) and Routing Information Protocol (RIP)
under given condition.
For each routing protocol, they get the throughtput of both TCP and UDP protocol in the condition that router 2 is up or fail.

2) Does the project generally follow the guidelines and parameters we have
learned in class?

Yes. The goal of the project is clear and easy to achieve.
The matrics and parameters are appropariate for the experiment.



## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?

This project compares the performance of OSPF and RIP under given condition.
It is a focused and specific goal, because it only compares two routing protocols in two situation
of two different protocols dividually.
It may get a useful result because it compares the performance of two routing protocols.

2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?

This project use OSPF and RIP as parameters and rtt, throughput, traceroute as metrics.
Since the experiment goal is the performance, the metrics is appropriate.

3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?

Yes. In this experiment, each trial can get the full information we need.

4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?

The metrics selected for study is the right metrics. The performance of OSPF and RIP include rtt, throughput and traceroute,
so the metrics is likely to lead to correct conclusions.

5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?

This project use OSPF and RIP as parameters. This two parameters are the most important elements in the projects.

6) Have the authors sufficiently addressed the possibility of interactions
between parameters?

The performance of the OSPF and RIP is independent and OSPF and RIP is also independent and do not have interactions.


7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?

The comparisons is performances between OSPF and RIP which is reasonably.


## Communicating results


1) Do the authors report the quantitative results of their experiment?

Yes. For each experiment, there are three trails which make sure the accuracy of the experience.

2) Is there information given about the variation and/or distribution of
experimental results?

Yes.

3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?

Yes. The data I duplicated in the experiment came out the same conclusion with the author. The data is directly used to analyse
and avoids ratio games and other practices to artificially make their results seem better

4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?

Yes. the data is presented in graphical form and the line graphic makes the data clear to illustrate.

5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?

The conclusion is that TCP connection gives much higher throughput than UDP connection, which is sufficiently supported by the
experiment results

## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?

Yes. First the author provides how to set up experiment topology and routers. Then the author gives the step by step instruction
to get data. Finally, the author tells how to analyse the data.
The instruction is clear and easy to understand.

2) Were you able to successfully produce experiment results?

Yes, the experiment is easily to reduplicate and get the same results with the author.

3) How long did it take you to run this experiment, from start to finish?

It takes me about 15 minutes to run this experiment.

4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?

No, everything that this experiment need involve in the instruction. No more extra step is needed.

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?

I think it belongs to degreed 5.


## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.

This project can have the same experiment in the different topology and measure the performance.
