
Project Review
=====================================================

## Overview

1) Briefly summarize the experiment in this project.
The experiment is to measure the performance of routing protocols OSPF and RIP using bandwidth and RTT as metrics to compare the routing protocols. A 4 node topology is used in this experiment  using Geni Testbeds. So the nodes are configured in the Geni Testbeds.
In the terminal data is collected for comparison. For RTT, traceroute command is used which gives the round trip time and throughput is also measured for both TCP and UDP connections using iperf command. Results are plotted using MS-Excel. When all the routers were up, the RTT
for OSPF was less as compared to RIP.  When one of the router was down in the routing path  each protocol had to find a new routing path from the client to the server. In this case also, performance of OSPF was better than RIP as OSPF took less time for rerouting from client to
the server. Analyzing the throughput plot also the performance of OSPF is slightly better than RIP.The authors concluded that OSPF has better performance overall as it has the lesser cost of transmission, lower router overhead than RIP and better throughput amongst the two.


2) Does the project generally follow the guidelines and parameters we have
learned in class?
Yes, the project follow the guidelines and parameters we learned in class. It uses RTT and bandwidth as metrics to compare the routing protocols OSPF( It uses Link State Algorithm. It is the most widely used Interior Gateway Protocol. It gathers information about its
neighboring routers only, and accordingly figures out the shortest path to reach the destination. It then chooses the best path that takes lesser time and lower cost to reach the destination)  and RIP(It uses Distance Vector Routing that follows Bellman Ford Algorithm. RIP has the
information about the entire topology and so it already knows what path to take in order to reach the destination. It provides great network stability).
The experiment has a good goal, appropriate metrics and meaningful conclusions. It is easy to understand and perform.



## Experiment design

1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?
The goal of the experiment is to compare the performance of RIP and OSPF using RTT and bandwidth as metrics to compare the routing
protocols. Yes, It is a specific goal to compare the routing protocols. Yes, we can get useful results about the performance of the
protocols which one is better.

2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?
The metric chosen in this experiment is RTT and Bandwidth. RTT is appropriate for the experiment goal as it clearly compares the routing protocols RIP and OSPF and
the results plotted states that OSPF is better than RIP. Bandwidth which is measured using iperf command in TCP and UDP connections.
Although routing protocols doesn't use TCP or UDP protocols still the author has calculated throughput with no variance in the results as the performance by
both TCP and UDP is the same.

3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?
Yes, It is well designed to obtain the maximum information with just one trial only in case of RTT but in case of throughput the author is not
varying anything and just calculating the bandwidth based on TCP and UDP connections which is not related to the goal of the experiment.

4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?
The metrics selected for the study are RTT and Bandwidth. RTT is the correct metric because it is tells us which routing method will take less time to reach
from the source to destination.
The author have used bandwidth to compare the two protocols TCP and UDP. Bandwidth may not be the right metric used to compare the two routing
protocols because the observed bandwidth for both TCP and UDP did not significantly change when routing protocol was changed.


5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?
The main parameter they have varied is the routing from client to server. It is meaningful and representative of the goal. It clearly shows the superiority of
OSPF over RIP for different paths and going from different routers, satisfying the design of the experiment.

6) Have the authors sufficiently addressed the possibility of interactions
between parameters?
The parameter chosen by the author is throughput which is not varied in order to compare the routing protocols. So, there is no scope of any interaction as
the parameters are independent.


7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic?
The comparison made on basis of RTT is appropriate as it clearly states the comparison between RIP and OSPF.The baseline selected is that OSPF performs better than RIP is justified because the authors observe
less RTT for OSPF than they do for RIP in both the cases- when all routers are working and when one router is taken down. But in the conclusion
the bandwidth considered by the author is not realistic and seems to be a little unreasonable as it is measured on the basis of UDP and TCP connections
in both RIP and OSPF and the results obtained are almost similar.


## Communicating results


1) Do the authors report the quantitative results of their experiment?
Yes, The authors do report the experiment results via screenshots. The results were not put in a separate file.
The data was easy to obtain with no extra effort and was effective.


2) Is there information given about the variation and/or distribution of
experimental results?
Yes, variation of the results was carefully noted down.For RTT there is information about the variation of experimental results as while plotting in from of line graphs
I got the variation in the results but there were no variation in the results when throughput was measured in case of UDP and TCP connections.


3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?
The data for RTT obtained is correct and is correctly plotted using the right results but in case of RIP the author havent used RIP as in the
configuration file of RIP the RIP code is # commented # and by measuring throughput the author is unable to justify which routing protocol is better
RIP or OSPF because the results obtained are almost similar.

4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling?
The data had to be manually typed into MS Excel to obtain any visualizations of the experiment results. This would seem fine if the recorded
data was less in size. But, for larger volumes of data, manually typing the results obtained may not be very feasible. It can be improved by migrating the
data to an outfile and then using that file to plot the graphs or any other viualizations needed.Instead of manually plotting the results in MS-EXCEL the
author should have used Rscript to give a better picture of the experimental results.
The visualizations were effective. The authors have used line plots to indicate the superiority of a particular protocol over the other.


5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?
The conclusion drawn by the authors is partially supported by the experiment results. It is efficiently proved that OSPF does perform better than RIP
considering the RTT they have measured. The RTT observed was quite less for OSPF for both scenarios.
The conclusion that OSPF has better performance overall as it has the lesser cost of transmission, lower router overhead than RIP and better throughput
amongst the two is not quite well supported because the authors have shown no throughput and router overhead variations in OSPF and RIP.


## Reproducible research



1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?
Yes, instructions for reproducing the experiment were included. The instructions were in a language that is easy to understand and each step is explained
very well.Everything from setting up the experiment to plotting the visualizations was communicated clearly.The author have not provided with any raw data
just gave the screenshots of the results obtained and in the experimental results the author just plotted manually using MS-EXCEL instead of using Rscript
thus giving an unclear experimental results.
The authors made it simple to reproduce the experiment since they had the environment ready.



2) Were you able to successfully produce experiment results?
Yes, I was able to successfully produce the experiment results. I had a little problem while manually noting down the RTT and bandwidth values
to plot them in MS Excel, but reproduction of the experiment was no problem.

3) How long did it take you to run this experiment, from start to finish?
It took 2 and an half hours to run the experiment from the start to the finish.

4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*. How long did these extra steps or changes take to figure out?
No, I didnt make any additional changes to complete the experiment. It is reproducable by the steps provided by the author.

5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility. How would you characterize this experiment - where does it fall on the six degrees of reproducibility?
It falls on number 3 of reproducibility- The results can be reproduced by an independent researcher, requiring considerable effort.



## Other comments to authors

Please write any other comments that you think might help the authors
of this project improve their experiment.
