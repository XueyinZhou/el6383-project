
Project Review
=====================================================


## Overview

**1) Briefly summarize the experiment in this project.**

- The authors have compared the performances of two routing protocols: OSPF and RIP to measure the Round Trip Time and Bandwidth
- They have followed a 4 node topology to find out routing information over the network.
- RTT was calculated from the client to the server by doing traceroute and the bandwidths were found using iperf for 2 types of transfer- TCP and UDP, for each protocol.
Similar run was done after one of the routers was taken down and each protocol had to find a new routing path from the client to the server. Data was collected from the terminal.
- The authors have used MS Excel to visualize their results i.e. plotting of RTT and bandwidths obtained after each run.
- After analyzing the result of the performance of RIP and OSPF over each scenario, the authors concluded that OSPF has better performance overall as it
has the lesser cost of transmission, lower router overhead than RIP and better throughput amongst the two.


**2) Does the project generally follow the guidelines and parameters we have
learned in class? **

- Yes, the project fairly follows the guidelines discussed in class to a certain extent.
The experiment has a good goal, appropriate metrics and meaningful conclusions.
- Metrics chosen are justified, although the author forgets to mention specifically about the parameters.
- It is easy to understand and perform requiring very few manual steps but data gathering and visualization could have been better.

--------------------

## Experiment design

**1) What is the goal of this experiment? Is it a focused, specific goal?
Is it useful and likely to have interesting results?**

- The goal of the experiment is to analyze and compute the different metrics for routing protocols and compare their results using parameters to
measure the performances of routing protocols OSPF and RIP.
- It could have been more specific, if the authors said exactly what metrics they would measure and what exact parameters and how much they would vary or not vary in the goal.


**2) Are the metric(s) and parameters chosen appropriate for this
experiment goal? Does the experiment design clearly support the experiment goal?**

- The authors chose to measure RTT and Bandwidth.
- For the given experiment design, RTT is the correct metric as it measures the total time a packet takes to travel from source to destination and back.
- The authors also record the bandwidth for TCP and UDP connections for each routing algorithm. This may not be a very suitable metric because the goal is to compare OSPF and RIP and
TCP and UDP bandwidths calculated do not show any variations over different routing algorithms.


**3) Is the experiment well designed to obtain maximum information with the
minimum number of trials?**

- Yes, the experiment design tells us exactly what to do to obtain the information in just one trial.


**4) Are the metrics selected for study the *right* metrics? Are they clear,
unambiguous, and likely to lead to correct conclusions? Are there other
metrics that might have been better suited for this experiment?**

- The metrics selected for the study are RTT and Bandwidth.
- RTT is the correct metric because it is tells us which routing method will take less time to reach from the source to destination.
- The authors have used bandwidth to compare the two protocols TCP and UDP. Bandwidth may not be the right metric used to compare the two routing protocols because
the observed bandwidth for both TCP and UDP did not significantly change when routing protocol was changed.
- The other metrics they could have used are: Link overhead and processing power.


**5) Are the parameters of the experiment meaningful? Are the ranges
over which parameters vary meaningful and representative?**

- The authors have cited Round Trip Time as their parameter but actually have used it as a metric.
- The main parameter they have varied is the path of traversal from client to server. It is meaningful and representative of the goal. It clearly shows the
superiority of OSPF over RIP for different paths and going from different routers, satisfying the design of the experiment.


**6) Have the authors sufficiently addressed the possibility of interactions
between parameters?**

- No, any possibility of interactions has not been mentioned.
- When a particular interface of a router is taken down, connection is still present through different interfaces of the router. This can possibly be an
interaction and may not describe the *'router down'* scenario completely.
- For eg, I had to note the routing table and RTT for the particular topology with Router 2 down. When I put down the link, the routing table still showed me
the router connected through another interface.


**7) Are comparisons made reasonably? Is the baseline selected for comparison appropriate
and realistic? **

- The comparisons are reasonable. The baseline selected is that OSPF performs better than RIP is justified well because the authors observe less RTT for OSPF than they do for RIP
in both the cases- when all routers are working and when one router is taken down.
- But the conclusion that the authors derive from calculating bandwidth seems to be a little unreasonable because the authors have measured the bandwidth
for TCP and UDP in both OSPF and RIP and both these cases have given similar results.
- The authors also say that after analyzing the performance of RIP and OSPF over a scenario for cost of transmission, router overhead, throughput, they found that OSPF has
better performance overall as it has the lesser cost of transmission, lower router overhead than RIP and better throughput amongst the two. But, they have not calculated these metrics they mentioned.

--------------------

## Communicating results

**1) Do the authors report the quantitative results of their experiment?**

- Yes, the authors do report the experiment results via screenshots. The results were not put in a separate file.
- The data was easy to obtain with no extra effort and was effective.


**2) Is there information given about the variation and/or distribution of
experimental results?**

- Yes, variation of the results was carefully noted down.
- Differences between bandwidths obtained from different protocols (TCP and UDP) are shown in the form of line graphs.
- Variations of RTT with respect to each network routing protocol was also effectively shown via line graphs.


**3) Do the authors practice *data integrity* - telling the truth about their data,
avoiding ratio games and other practices to artificially make their results seem better?**

- The data given by the authors is true. It is recorded exactly as intended _assuming that the OSPF and RIP configurations is true._
- Similar data was observed when I reproduced the experiment given the settings.


**4) Is the data presented in a clear and effective way? If the data is presented in
graphical form, is the type of graph selected appropriate for the "story" that
the data is telling? **

- The data had to be manually typed into MS Excel to obtain any visualizations of the experiment results. This would seem fine if the recorded data was less in size.
But, for larger volumes of data, manually typing the results obtained may not be very feasible. It can be improved by migrating the data to an outfile and then using that file to
plot the graphs or any other viualizations needed.
- The visualizations were effective. The authors have used line plots to indicate the superiority of a particular protocol over the other. Line graphs helped to show
changes in the RTT over time and demonstrate trends in bandwidth for TCP and UDP.
- From the line plot, I could clearly tell that the RTT obtained for OSPF is less that that for RIP.


**5) Are the conclusions drawn by the authors sufficiently supported by the
experiment results?**

- The conclusion drawn by the authors is partially supported by the experiment results. It is efficiently proved that **OSPF does perform better than RIP considering the RTT**
they have measured. The RTT observed was quite less for OSPF for both scenarios.
- The conclusion that **OSPF has better performance overall because it has the lesser cost of transmission, lower router overhead than RIP and better throughput amongst the two** is not
quite well supported because the authors have shown no throughput and router overhead variations in OSPF and RIP.

--------------------

## Reproducible research

**1) Did the authors include instructions for reproducing the experiment in 3 ways (Raw data -> Results,
Existing experiment setup -> Data, and Set up experiment)? Are the instructions clear
and easy to understand?**

- Yes, instructions for reproducing the experiment were included. The instructions were in a language that is easy to understand and each step is explained very well.
- Everything from setting up the experiment to plotting the visualizations was communicated cogently.
- The authors made it simple to reproduce the experiment since they had the environment ready.


**2) Were you able to successfully produce experiment results? **

- Yes, I was able to successfully produce the experiment results. I had a little trouble when I had to manually note down the RTT and Bandwidth values to plot them in MS Excel, but
reproduction of the experiment was no problem.


**3) How long did it take you to run this experiment, from start to finish?**

- It took me 3 hours to run this experiment from start to finish.


**4) Did you need to make any changes or do any additional steps beyond the documentation in order to successfully complete this experiment? Describe *in detail*.
How long did these extra steps or changes take to figure out?**

- No, I did not take any additional changes to complete the experiment. It is completely reproducible by the steps provided by the author.


**5) In the [lecture on reproducible experiments](http://witestlab.poly.edu/~ffund/el6383/files/Reproducible+experiments.pdf), we mentioned six degrees of reproducibility.
How would you characterize this experiment - where does it fall on the six degrees of reproducibility?**

- Degree of reproducibility : 4

--------------------

## Other comments to authors

**Please write any other comments that you think might help the authors
of this project improve their experiment.**

+ **Design:**
	- The experiment was designed well and efficient.
	- The topology selected was representative of a scalable network.
	- The introduction is relevant and theory based.
	- Improvement can be done if authors explain how the bandwidth of TCP and UDP connections affects the performance of the OSPF and RIP routing protocols.
	- A more thorough explanation of the configuration of RIP and OSPF protocols can be provided. In the configuration file provided, the code for RIP was `/*commented*/`
    so I was not sure whether RIP was really activated or not.

+ **Communicating Results:**
	- Screenshots of the obtained results was provided. To make the experiment more reproducible, it would have been better if they were recorded
	and stored in a separate file for more detailed processing.
	- Instead of manually entering the observed data in MS Excel and then plotting different graphs, the authors could have created a script that would create intelligent visualizations.
